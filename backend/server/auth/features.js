import passport from "passport";
import { Strategy, ExtractJwt } from "passport-jwt";
import jwt from "jsonwebtoken";

import { JWT_SECRET } from "../config";
import User from "../modul/user/model";

const verFunc = async (payload, done) => {
  try {
    const user = await User.findById(payload.id);
    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
    }
  } catch (error) {
    return done(error, false);
  }
};

const newStrategy = new Strategy(
  {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
    secretOrKey: JWT_SECRET
  },
  verFunc
);

passport.use(newStrategy);

// EXPORT

export const requireJwtAuth = passport.authenticate("jwt", { session: false });

export const createToken = args => jwt.sign({ id: args._id }, JWT_SECRET);
