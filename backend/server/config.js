// DB online
export const URL = 'mongodb://firstUser:firstUser123@ds139193.mlab.com:39193/nature';
// export const URL = 'mongodb://localhost/nature';

// Authentication secret
export const JWT_SECRET = "janhrdy";

// Port
export const PORT = process.env.PORT || 3000;