import mongoose from 'mongoose';
import {URL} from './config';

export default () => {
  // mongoose.Promise = global.Promise;
  mongoose.connect(URL, {useMongoClient: true});
  mongoose.connection
    .once('open', () => console.log('DB is working'))
    .on('error', err => console.error(err));
};