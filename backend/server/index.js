import express from "express";
import dbConfig from "./db";
import { PORT } from "./config";
import middlewares from "./config/middlewares";
import {
  BasicRoutes,
  CultureMonumentRoutes,
  CultureCategoryRoutes,
  UserRoutes
} from "./modul";

const app = express();

// Database
dbConfig();

// Middleware
middlewares(app);

app.use("/api", [
  BasicRoutes,
  CultureMonumentRoutes,
  CultureCategoryRoutes,
  UserRoutes
]);

app.listen(PORT, err => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Listen to PORT: ${PORT}`);
  }
});
