import BasicModel from "./model";

export const createBasic = async (req, res) => {
  const { title, description } = req.body;
  const newBasic = new BasicModel({
    title,
    description
  });

  try {
    return res.status(201).json({
      basics: await newBasic.save()
    });
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Something bad happended in mehtod createBasic"
    });
  }
};

export const getAllBasic = async (req, res) => {
  try {
    return res.status(200).json({ basics: await BasicModel.find({}) });
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Something bad happended in mehtod getAllBasic"
    });
  }
};