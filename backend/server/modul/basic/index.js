import Basic from './model';
import BasicRoutes from './routes';

export {
  BasicRoutes,
  Basic,
};