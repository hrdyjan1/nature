import mongoose, {
    Schema
} from 'mongoose';

const BasicSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
})

export default mongoose.model('Basic', BasicSchema)