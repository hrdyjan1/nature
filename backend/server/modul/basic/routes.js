import { Router } from "express";
import * as BasicController from "./controller";

const routes = new Router();

routes.post("/basics", BasicController.createBasic);
routes.get("/basics", BasicController.getAllBasic);

export default routes;
