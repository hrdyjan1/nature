import CultureCategoryModel from "./model";
import {
  createCultureMonument,
  CultureMonumentModel
} from "../cultureMonument";

import mongoose from "mongoose";

export const createCultureCategory = async (req, res) => {
  const { title, description } = req.body;
  const newCultureCategory = new CultureCategoryModel({
    _id: mongoose.Types.ObjectId(),
    title,
    description
  });

  if (!title || !description) {
    return res
      .status(400)
      .json({ error: true, message: "Missing title or description." });
  }

  try {
    return res.status(201).json({
      CultureCategory: await newCultureCategory.save()
    });
  } catch (error) {
    return res.status(e.status).json({
      error: true,
      message: "Something bad happended in mehtod createBasic"
    });
  }
};

export const getAllCultureCategory = async (req, res) => {
  try {
    return res
      .status(200)
      .json({ CultureCategory: await CultureCategoryModel.find({}) });
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Something bad happended in mehtod getAllCultureCategory"
    });
  }
};

export const createCultureCategoryMonument = async (req, res) => {
  const { cultureCategoryId } = req.params;
  let category;

  try {
    category = await CultureCategoryModel.findById(cultureCategoryId);
  } catch (error) {
    return res.status(404).json({
      error: true,
      message: "Cannot find category of that id"
    });
  }

  req.body.category = category;

  createCultureMonument(req, res);
};

export const getAllCultureMonuments = async (req, res) => {
  const { cultureCategoryId } = req.params;

  // Check if CultureCategory exists
  try {
    await CultureCategoryModel.findById(cultureCategoryId);
  } catch (error) {
    return res.status(404).json({
      error: true,
      message: "Cannot find category of that id"
    });
  }

  // Find all monuments in category with id: cultureCategoryId
  try {
    return res.status(200).json({
      CultureMonument: await CultureMonumentModel.find({
        category: cultureCategoryId
      }).populate("CultureCategory")
    });
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Category id exists, but then something bad happended"
    });
  }
};
