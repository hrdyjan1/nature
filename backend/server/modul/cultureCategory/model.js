import mongoose, { Schema } from "mongoose";

const CultureCategorySchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: String,
  description: String
});

export default mongoose.model("CultureCategory", CultureCategorySchema);
