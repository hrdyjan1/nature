import { Router } from "express";

import * as CultureCategoryController from "./controller";
import { requireJwtAuth } from "../../auth/features";

const routes = new Router();

routes.get("/CultureCategory", CultureCategoryController.getAllCultureCategory);

routes.post(
  "/CultureCategory/new",
  requireJwtAuth,
  CultureCategoryController.createCultureCategory
);

routes.get(
  "/CultureCategory/:cultureCategoryId/cultureMonument",
  CultureCategoryController.getAllCultureMonuments
);

routes.post(
  "/CultureCategory/:cultureCategoryId/cultureMonument/new",
  requireJwtAuth,
  CultureCategoryController.createCultureCategoryMonument
);

export default routes;
