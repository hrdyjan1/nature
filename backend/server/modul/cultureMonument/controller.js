import CultureMonumentModel from "./model";
import { UserModel } from "../user";

export const createCultureMonument = async (req, res) => {
  const {
    title,
    description,
    likes,
    author,
    category,
    authorID,
    latitude,
    longitude
  } = req.body;

  if (!title || !category) {
    return res
      .status(400)
      .json({ error: true, message: "Missing title or category." });
  }

  if (!latitude || !longitude) {
    return res
      .status(400)
      .json({ error: true, message: "Missing latitude or longitude." });
  }

  if (!author || !authorID) {
    return res.status(400).json({ error: true, message: "Missing author." });
  }

  const newCultureMonument = new CultureMonumentModel({
    title,
    description,
    likes,
    author,
    authorID,
    latitude,
    longitude,
    category
  });

  try {
    const newMonument = await newCultureMonument.save();
    const sendedMonument = await res.status(201).json({
      CultureMonuments: newMonument
    });

    // Check if succesfylly added, than save to users monuementIDs
    if (sendedMonument.statusCode === 201) {
      UserModel.findByIdAndUpdate(
        authorID,
        { $push: { monumentsId: newMonument._id } },
        { safe: true, upsert: true, new: true },
        function(err, model) {
          console.log(err);
        }
      );
    }
  } catch (error) {
    return res.status(error.status).json({
      error: true,
      message: "Something bad happended in mehtod createBasic"
    });
  }
};

export const removeCultureMonument = async (req, res) => {
  const { _id } = req.body;

  if (!_id) {
    return res
      .status(400)
      .json({ error: true, message: "Missing _id in monument." });
  }

  try {
    const deletedMonument = await CultureMonumentModel.findByIdAndRemove({
      _id
    });

    if (!deletedMonument) {
      return res.status(404).json({
        success: false,
        message: "Monument not found or already deleted "
      });
    } else {
      UserModel.findByIdAndUpdate(
        deletedMonument.authorID,
        { $pull: { monumentsId: deletedMonument._id } },
        function(err) {
          if (err) {
            return res.status(500).json({
              success: false,
              message: "Problem with deleting monumentId from user."
            });
          } else {
            return res.status(200).json({
              success: true,
              message: "Deleted monument even from user and monuments so."
            });
          }
        }
      );
    }
  } catch (error) {
    return res.status(error.status).json({
      error: true,
      message: "Something bad happended in mehtod removeCultureMonument"
    });
  }
};

export const getCultureMonumentsById = async (req, res) => {
  // Ugly code

  // Declaration
  let monumentsID;
  let headerMonuments = req.get("Monuments") || "";

  // Check if monuments not empty
  // True <- Even if the headerMonuments is empty array
  if (headerMonuments === "") {
    return res.status(404).json({
      error: true,
      message: "Monuments empty",
      monuments: []
    });
  }

  // Make Array of monuments
  if (Array.isArray(headerMonuments)) {
    monumentsID = headerMonuments;
  } else {
    monumentsID = headerMonuments.split(",").map(n => n);
  }

  try {
    if (monumentsID) {
      const newMonumentsID = await monumentsID.map(
        async id => await CultureMonumentModel.findById(id)
      );

      Promise.all(newMonumentsID).then(completed => {
        return res.status(200).json({ error: false, monuments: completed });
      });
    }
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Problem with finding all monuments."
    });
  }
};

export const getCultureMonuments = async (req, res) => {
  try {
    CultureMonumentModel.find({}).then(function(monuments) {
      return res.status(200).json({ error: false, monuments: monuments });
    });
  } catch (error) {
    return res.status(error.status).json({
      error: true,
      message: "Problem with finding all monuments."
    });
  }
};
