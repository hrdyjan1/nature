import CultureMonumentRoutes from "./routes";
import { createCultureMonument } from "./controller";
import CultureMonumentModel from "./model";

export { CultureMonumentRoutes, createCultureMonument, CultureMonumentModel };
