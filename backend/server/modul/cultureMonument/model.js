import mongoose, { Schema } from "mongoose";

const CultureMonumentSchema = new Schema({
  title: String,
  description: String,
  likes: { type: Number, default: 0 },
  author: String,
  authorID: String,
  latitude: Number,
  longitude: Number,
  category: {
    type: Schema.Types.ObjectId,
    ref: "CultureCategory"
  }
});

export default mongoose.model("CultureMonument", CultureMonumentSchema);
