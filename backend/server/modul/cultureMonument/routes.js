import { Router } from "express";
import * as CultureMonumentController from "./controller";
import { requireJwtAuth } from "../../auth/features";

const routes = new Router();

routes.post(
  "/cultureMonuments/new",
  CultureMonumentController.createCultureMonument
);

routes.delete(
  "/CultureMonument/remove",
  requireJwtAuth,
  CultureMonumentController.removeCultureMonument
);

routes.get(
  "/CultureMonument",
  CultureMonumentController.getCultureMonumentsById
);

routes.get("/CultureMonuments", CultureMonumentController.getCultureMonuments);

export default routes;
