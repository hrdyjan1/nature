import User from "./model";
import { createToken } from "../../auth/features";

export const loginAuthentication = async (req, res) => {
  try {
    // Missing user in body
    if (!req.body.user) {
      throw new Error("Missing User, fail in loginAuthenticaiton on server");
    }

    const { email, name, password = null } = req.body.user;

    if (!email) {
      throw new Error("Missing email, fail in loginAuthenticaiton on server");
    }

    // Check if registration
    if (password && name) {
      const newUser = await User.create({ email, name, password });
      newUser.password = undefined;

      return res.status(200).json({
        user: newUser,
        success: true,
        token: `JWT ${createToken(newUser)}`
      });
      // Check if user log in via password
    } else if (password && email) {
      try {
        const newUser = await User.findOne({ email, password });
        if (newUser) {
          newUser.password = undefined;

          return res.status(200).json({
            user: newUser,
            success: true,
            token: `JWT ${createToken(newUser)}`
          });
        } else {
          throw new Error(
            "Wrong password or email, fail in loginAuthenticaiton on server"
          );
        }
      } catch (err) {
        return res.status(400).json({
          success: false,
          errorName: err.name,
          errorMessage: err.message
        });
      }
    }

    let newUser = await User.findOne({ email }, function(err, user) {
      if (err) {
        return done(err);
      }

      //No user was found... so create a new user
      if (!user) {
        return null;
      } else {
        //found user. Return
        return user;
      }
    });

    if (!newUser) {
      let name = name ? name : "Anonymous";
      newUser = await User.create({ email, name });
    }

    // Return success
    return res.status(200).json({
      user: newUser,
      success: true,
      token: `JWT ${createToken(newUser)}`
    });
  } catch (err) {
    return res.status(400).json({
      success: false,
      errorName: err.name,
      errorMessage: err.message
    });
  }
};

export const getUserNameByID = async (req, res) => {
  // Missing user in body
  if (!req.header("id")) {
    return res.status(404).json({
      error: true,
      message: "Missing user id, fail in getUserByID on server"
    });
  }

  const id = req.header("id");
  // const id = "5c03dab9a5e6a40016517b89";

  try {
    return res
      .status(200)
      .json({ user: await User.findById(id).select("name") });
  } catch (e) {
    return res.status(e.status).json({
      error: true,
      message: "Something bad happended in mehtod getUserByID"
    });
  }
};

export const getAllUsers = async (req, res) => {
  User.find({}, function(err, users) {
    var userMap = {};

    users.forEach(function(user) {
      userMap[user._id] = user;
    });

    return res.status(200).json({
      users: userMap
    });
  });
};
