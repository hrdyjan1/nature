import UserRoutes from "./routes";
import { loginAuthentication, newLoginAuthentication } from "./controller";
import UserModel from "./model";

export { UserRoutes, loginAuthentication, newLoginAuthentication, UserModel };
