import mongoose, { Schema } from "mongoose";

const UserSchema = new Schema({
  name: String,
  email: String,
  password: { type: String, default: null, required: false },
  monumentsId: [String]
});

export default mongoose.model("User", UserSchema);
