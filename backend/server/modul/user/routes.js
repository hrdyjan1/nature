import { Router } from "express";

import * as UserController from "./controller";

const routes = new Router();

// routes.post("/users/auth", UserController.loginAuthentication);
routes.post("/users/auth", UserController.loginAuthentication);
routes.get("/user", UserController.getUserNameByID);
// routes.get("/users", UserController.getAllUsers);

export default routes;
