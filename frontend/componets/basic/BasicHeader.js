/**
 * @file Component used like template header
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Header } from "react-native-elements";

// Redux
import { connect } from "react-redux";

// Features
import i18n from "../../features/I18n";
import colors from "../../features/colors";

// Components
import BasicIcon from "./BasicIcon";

const BasicHeader = ({ leftIcon, rightIcon, rightMethod, leftMethod }) => (
  <Header
    barStyle="light-content"
    placement="center"
    backgroundColor={colors.lightGreen}
    leftComponent={
      leftIcon && <BasicIcon icon={leftIcon} onPress={leftMethod} />
    }
    centerComponent={{
      text: i18n.t("basic.title"),
      style: { fontSize: 24, color: "#fff" }
    }}
    rightComponent={
      rightIcon && <BasicIcon icon={rightIcon} onPress={rightMethod} />
    }
  />
);

const mapStateToProps = state => ({
  i18n: state.i18n
});

export default connect(mapStateToProps)(BasicHeader);
