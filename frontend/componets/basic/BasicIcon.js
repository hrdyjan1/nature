/**
 * @file Component for icon
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function notYetImplemented() {
  alert("Not yet implemented");
}

const BasicIcon = ({ icon, fontType, color, onPress }) => (
  <Icon
    raised
    size={22}
    name={icon || "menu"}
    type={fontType || "material-community"}
    color={color || "#fff"}
    onPress={onPress || notYetImplemented}
  />
);

export default BasicIcon;
