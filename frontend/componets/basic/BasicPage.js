/**
 * @file Component used like template for screens
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Text, View } from "react-native";

// Redux
import { connect } from "react-redux";

// Features
import i18n from "../../features/I18n";
import styles from "./style";

// Components
import BasicHeader from "../../componets/basic/BasicHeader";
import OrdinaryButton from "../buttons/OdinaryButton";
import LoadingScreen from "../../screens/loading/LoadingScreen";

const BasicPage = props => {
  const {
    isLogginRequire,
    isUserLogged,
    icons,
    methods,
    navigation,
    children,
    loading
  } = props;

  if (loading) {
    return (
      <View style={styles.container}>
        <BasicHeader />
        <LoadingScreen />
      </View>
    );
  } else if (isLogginRequire && !isUserLogged) {
    return (
      <View style={styles.container}>
        <BasicHeader
          leftIcon={icons.leftIcon}
          rightIcon={icons.rightIcon}
          leftMethod={methods.leftIconMethod}
          rightMethod={methods.rightIconMethod}
        />

        <View style={styles.center}>
          <Text style={styles.centerText}>{i18n.t("basic.loginRequier")}</Text>
        </View>

        <OrdinaryButton
          name={i18n.t("basic.goHome")}
          method={() => navigation.navigate("Home")}
        />
        <OrdinaryButton
          name={i18n.t("basic.signIn")}
          method={() => navigation.navigate("SignIn")}
        />
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <BasicHeader
          leftIcon={icons.leftIcon}
          rightIcon={icons.rightIcon}
          leftMethod={methods.leftIconMethod}
          rightMethod={methods.rightIconMethod}
        />
        <View style={styles.container}>{children}</View>
      </View>
    );
  }
};

const mapStateToProps = state => ({
  i18n: state.i18n
});

export default connect(mapStateToProps)(BasicPage);
