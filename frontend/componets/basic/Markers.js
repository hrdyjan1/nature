/**
 * @file Component with all markers in map
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Text, View } from "react-native";
import MapView, { Marker } from "react-native-maps";

// Redux
import { connect } from "react-redux";

// Features
import i18n from "../../features/I18n";

// Components
import OdinaryButton from "../buttons/OdinaryButton";

function navigateToSingleMonument(navigate, monument) {
  navigate("SingleMonument", {
    monument: { ...monument }
  });
}

const Markers = ({
  rootMonument,
  rootCategory,
  navigate,
  likeSingleMonument
}) =>
  rootMonument &&
  rootMonument.list.map(monument => {
    let newCategory = {};
    const { category } = rootCategory;
    category.forEach(singleCategory => {
      if (
        singleCategory._id == monument.category ||
        singleCategory._id == monument.category._id
      ) {
        newCategory = singleCategory;
      }
    });
    if (!newCategory.isChecked) {
      return null;
    }
    return (
      // Correct! Key should be specified inside the array.
      <Marker
        key={monument._id}
        pinColor={newCategory.color || "#fff"}
        coordinate={{
          latitude: monument.latitude,
          longitude: monument.longitude
        }}
      >
        <MapView.Callout>
          <View>
            <Text>{monument.title}</Text>
            <Text>{monument.description}</Text>
            <Text>Category : {newCategory.title}</Text>
            <OdinaryButton
              name={i18n.t("home.like")}
              method={() => likeSingleMonument(monument)}
            />
            <OdinaryButton
              name={i18n.t("home.more")}
              method={() => navigateToSingleMonument(navigate, monument)}
            />
          </View>
        </MapView.Callout>
      </Marker>
    );
  });

const mapStateToProps = state => ({
  i18n: state.i18n
});

export default connect(mapStateToProps)(Markers);
