/**
 * @file Style for templates
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";
import colors from "../../features/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  centerText: {
    textAlign: "center"
  },

  // HomeScreen
  mapView: {
    flex: 1,
    marginVertical: 10,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: colors.darkGreen
  }
});

export default styles;
