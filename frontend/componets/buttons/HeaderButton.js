/**
 * @file Bigger button for apliacition
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Button } from "react-native-elements";

function notYetImplemented() {
  alert("Not yet implemented");
}

const HeaderButton = props => (
  <Button
    raised
    large
    icon={{
      name: props.name || "home",
      style: { flex: 1, color: props.color || "red" },
      type: props.fontType || "material-community"
    }}
    onPress={props.method || notYetImplemented}
    buttonStyle={{
      // Default flexDirection is row
      padding: 0,
      backgroundColor: "transparent",
      alignItems: "center",
      justifyContent: "center"
    }}
  />
);

export default HeaderButton;
