/**
 * @file Normal button for apliaction
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Button } from "react-native-elements";

import colors from "../../features/colors";

function notYetImplemented() {
  alert("Not yet implemented");
}

const OdinaryButton = ({
  name,
  color,
  method,
  iconName,
  fontType,
  style,
  loading
}) => {
  const icon = iconName
    ? { name: iconName, type: fontType || "material-community" }
    : null;

  return (
    <Button
      // raised
      title={name || "Undefined"}
      onPress={method || notYetImplemented}
      buttonStyle={[
        { backgroundColor: color || colors.gray, margin: 5 },
        style
      ]}
      icon={icon}
      loading={loading}
    />
  );
};

export default OdinaryButton;
