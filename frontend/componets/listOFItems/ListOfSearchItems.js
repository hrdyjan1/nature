/**
 * @file Component for showing list of something
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { View, FlatList, StyleSheet } from "react-native";

import colors from "../../features/colors";
import OdinaryButton from "../buttons/OdinaryButton";

const ListOfSearchItems = ({ data, navigation, numberOfColumens = 2 }) => {
  return (
    <View style={{ flex: 1, alignContent: "stretch" }}>
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <View style={styles.categoryButtonStyle}>
            <OdinaryButton
              name={item.title}
              color={colors.lightGreen}
              style={{ margin: 0, height: 80 }}
              method={() =>
                navigation.navigate("ListOfMonument", {
                  category: item
                })
              }
            />
          </View>
        )}
        numColumns={numberOfColumens}
        keyExtractor={({ _id }, index) => _id}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  categoryButtonStyle: {
    flex: 1,
    height: 100,
    justifyContent: "center"
  }
});

export default ListOfSearchItems;
