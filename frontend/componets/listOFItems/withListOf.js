/**
 * @file HOC for categories in info screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";

import LoadingScreen from "../../screens/loading/LoadingScreen";

const withListOf = nameOfListOfProps => ListComponent => props => {
  const specificPropObject = props[nameOfListOfProps] || {};
  const list = specificPropObject.category || props.list;

  if (!list) {
    //   //   return <ErrorViewComponent />;
  } else if (specificPropObject.loading) {
    return <LoadingScreen />;
  } else {
    return (
      <ListComponent
        list={list}
        switchCategoryIsChecked={props.switchCategoryIsChecked}
      />
    );
  }
};

export default withListOf;
