/**
 * @file Tab bottom navigation icon
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import colors from "../../features/colors";

export default class TabBarIcon extends React.Component {
  render() {
    return (
      <Icon
        name={this.props.name}
        size={this.props.size}
        color={
          this.props.focused
            ? colors.primaryColor
            : this.props.defaultColor || colors.white
        }
      />
    );
  }
}
