/**
 * @file Setting up languages
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import I18n, { setLocale, Loc } from "react-native-redux-i18n";
import translations from "./translations";

I18n.fallbacks = true;
I18n.translations = translations;

export { setLocale, Loc };
export default I18n;
