/**
 * @file Api for connection with server
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import axios from "axios";

export const defaultURL = "https://nature-heroku.herokuapp.com/api";

export const createMonument = (monumentInfo, userToken) => {
  let { categoryID, ...CorrectMonumentInfo } = monumentInfo;

  const adress =
    defaultURL + "/CultureCategory/" + categoryID + "/cultureMonument/new";

  return fetch(adress, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: userToken
    },
    body: JSON.stringify(CorrectMonumentInfo)
  });
};

export const getAllCultureCateogory = () => {
  return fetch(defaultURL + "/CultureCategory")
    .then(response => response.json())
    .then(responseJson => {
      return responseJson.CultureCategory;
    })
    .catch(error => {
      console.error(error);
    });
};

/**
 * @param {string} id
 * @returns Array Of Monuments from caterogry with ID
 */
export const getAllCultureMonumentFromID = id => {
  const adress = defaultURL + "/CultureCategory/" + id + "/cultureMonument";

  if (!id) {
    return Promise.reject(
      new Error("Missing id in getllCultureMonumentFromID")
    );
  }

  return fetch(adress)
    .then(response => response.json())
    .then(responseJsonArray => {
      return responseJsonArray.CultureMonument;
    })
    .catch(error => {
      console.error(error);
    });
};

/**
 * @returns Array Of Promises Of Array of Monument
 */
export const getAllCultureMonument = () => {
  return getAllCultureCateogory()
    .then(arrayItems => {
      return arrayItems.map(item => {
        return item._id;
      });
    })
    .then(arrayOfID => {
      return arrayOfID.map(id => {
        return getAllCultureMonumentFromID(id);
      });
    })
    .then(arrayOfPromiseOfArrayOfMonument => {
      return arrayOfPromiseOfArrayOfMonument.map(promiseOfArrayOfMonument => {
        return promiseOfArrayOfMonument.then(arrayOfMonument => {
          return arrayOfMonument.map(item => {
            return item;
          });
        });
      });
    })
    .catch(error => {
      console.error(error);
    });

  // wtf.then(arrayPromisesofArrayOfMomument => {
  //   arrayPromisesofArrayOfMomument.map(promiseOfArrayOFMonument => {
  //     promiseOfArrayOFMonument.then(ArrayOFMonument => {
  //       console.log("ArrayOFMonument");
  //       console.log(ArrayOFMonument);
  //     });
  //   });
  // });
};

export const loginUserAPI = async (googleToken = null, args) => {
  let user = {};

  // Make user data
  try {
    if (googleToken) {
      user = await _loginUserGoogleAPI(googleToken);
    } else {
      user = await _loginUserNormalAPI(args);
    }
  } catch (error) {
    throw new Error(error);
  }

  // Send user data to server
  try {
    if (!user) {
      throw new Error("Missing user");
    }
    const res = await fetch(
      "https://nature-heroku.herokuapp.com/api/users/auth",
      {
        method: "POST", // or 'PUT'
        body: JSON.stringify({ user }),
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
      .then(res => res.json())
      .catch(error => {
        throw new Error(error);
      });

    if (!res || res.errorMessage) {
      throw new Error(res.errorMessage);
    } else {
      return { ...res, user: { ...res.user, ...user } };
    }
  } catch (error) {
    throw new Error(error.message);
  }
};

export const _loginUserGoogleAPI = async googleToken => {
  try {
    const { data } = await axios.get(
      "https://www.googleapis.com/userinfo/v2/me",
      {
        headers: { Authorization: `Bearer ${googleToken}` }
      }
    );
    if (!data) {
      throw new Error("Missing User Data, fail in loginUser via Google");
    }

    return data;
  } catch (error) {
    throw new Error({ error });
  }
};

// TODO
export const _loginUserNormalAPI = async args => {
  if (args.firstName) {
    return { ...args, name: `${args.firstName} ${args.lastName}` };
  } else {
    return { ...args };
  }
};

export const addIsChecked = array => {
  array.forEach(item => {
    item.isChecked = true;
  });
};
