/**
 * @file Communication with server connected with monuments
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { defaultURL } from "../api";

/**
 * @returns Object with key monuments = array of Monuments
 */
export const getAllMonuments = () => {
  const adress = defaultURL + "/CultureMonuments";

  return fetch(adress)
    .then(response => response.json())
    .catch(error => {
      console.error(error);
    });
};

export const deleteMonument = (monument, userToken) => {
  const adress = defaultURL + "/CultureMonument/remove";

  return fetch(adress, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: userToken
    },
    body: JSON.stringify(monument)
  });
};
