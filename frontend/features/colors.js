/**
 * @file List of all colors for apliacation
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

const primaryColor = "#334547"; // GRAY
const secundaryColor = "#fcc90d"; // YELLOW
const mainColor = "#4ac2d1"; // BLUE
const white = "#fff";

export default {
  mainColor,
  primaryColor,
  secundaryColor,

  // Challenges
  success: "#009432",
  successText: mainColor,
  error: "#EA2027",
  errorText: mainColor,
  normal: white,
  normalText: "#ccc",

  // Screens
  backgroundDefault: white,

  // Navigation
  tabIconSelected: secundaryColor,

  // Colors palete
  darkGreen: "#598234",
  lightGreen: "#AEBD38",
  gray: "#68829E",

  white: white,
  lightBlue: "#edffff"
};
