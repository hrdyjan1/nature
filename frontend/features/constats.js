/**
 * @file Constants for sign up and sign in
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

export const FIRSTSCREENAME = "Home";
export const SECONDSCREENAME = "Category";

export const GOOGLEINFO = {
  IOSID:
    "323242777987-ig3jphshq6ad33d5s259ks8rdrqf1rh1.apps.googleusercontent.com",
  ANDROIDID:
    "323242777987-frefaon552u65nfuqjlor25324ues1tj.apps.googleusercontent.com",
  GOOGLEERROR: "Problem with google login."
};
