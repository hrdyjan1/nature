export default {
  en: {
    info: {
      languages: "Languages",
      informations: "Informations",
      filters: "Filters",
      language_en: "English",
      language_cs: "Czech"
    },
    home: {
      like: "Like it",
      likeHeading: "Nice",
      more: "More information",
      likeText: "You liked it.",
      possition: "Show your position",
      newMonumentHeading: "New Monument",
      newMonumentText: "Would you like to add here something?",
      locationLoading: "Loading own location...",
      locationPermission:
        "For navigition needs to be allowed permission location."
    },
    basic: {
      name: "Name",
      email: "Email",
      firstName: "First name",
      lastName: "Last name",
      password: "Password",
      wellcome: "Wellcome",
      signUp: "Sign up",
      signIn: "Sign in",
      anonymous: "Anonymous",
      title: "Nature trails",
      googleConnection: "Connect with Gmail",
      loginRequier: "You must be logged in to access.",
      logoutText: "You were logout.",
      goHome: "Go home",
      goBack: "Go back",
      yes: "Yes",
      no: "No"
    },
    monument: {
      title: "Title",
      author: "Author",
      description: "Description",
      addMonument: "Add new monument",
      removeHeading: "Remove",
      removeText: "Would you really like to remove this monument?",
      category: "Category",
      likes: "Likes"
    }
  },
  cs: {
    info: {
      languages: "Jazyky",
      informations: "Informace",
      filters: "Filtry",
      language_en: "anglicky",
      language_cs: "česky"
    },
    home: {
      like: "To se mi líbí",
      likeHeading: "Paráda",
      more: "Více informací",
      possition: "Přejít na vlasní pozici",
      likeText: "Dal jsi to se mi líbí.",
      newMonumentHeading: "Nová památka",
      newMonumentText: "Chtěl by jste zde něco přidat?",
      locationLoading: "Načítání vlastní pozice ...",
      locationPermission:
        "Pro používání navigace musí být povolen přístup k lokaci."
    },
    basic: {
      name: "Jméno",
      email: "Email",
      firstName: "Jméno",
      lastName: "Přijímení",
      password: "Heslo",
      wellcome: "Vítejte",
      signUp: "Registrace",
      signIn: "Přihlášení",
      anonymous: "Anonym",
      title: "Naučné stezky",
      googleConnection: "Připojit se přes Gmail",
      loginRequier: "Pro přístup musíte být přihlášeni.",
      logoutText: "Byl jsi odhlášen.",
      goHome: "Domů",
      goBack: "Zpět",
      yes: "Ano",
      no: "Ne"
    },
    monument: {
      title: "Název",
      author: "Autor",
      description: "Popis",
      addMonument: "Přidat nové místo",
      removeHeading: "Odstranit",
      removeText: "Opravdu chcete odstranit tuto památku?",
      category: "Kategorie",
      likes: "To se mi líbí"
    }
  },
  _version: "1.0" // (you should) use `_version` if you plan to `setTranslations`(update) in-app
};
