/**
 * @file Navigation of the whole project - frontend
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { createSwitchNavigator, createStackNavigator } from "react-navigation";

import LoginScreen from "../screens/login/LoginScreen";
import SignInScreen from "../screens/login/SignInScreen";
import SignUpScreen from "../screens/login/SignUpScreen";
import MainTabNavigator from "./MainTabNavigator";

const LoginStack = createStackNavigator(
  {
    Login: LoginScreen,
    SignIn: SignInScreen,
    SignUp: SignUpScreen
  },
  {
    initialRouteName: "Login"
  }
);

export default createSwitchNavigator(
  {
    Main: MainTabNavigator,
    LoginPage: LoginStack
  },
  {
    initialRouteName: "LoginPage"
  }
);
