/**
 * @file Navigation for user and map screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { createStackNavigator } from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";

// Components

import TabBarIcon from "../componets/navigation/TabBarIcon";

// Colors

import colors from "../features/colors";

// Screens

import HomeScreen from "../screens/home/HomeScreenNew";
import NewMonumentScreen from "../screens/monument/NewMonumentScreen";
import SingleMonumentScreen from "../screens/monument/SingleMonumentScreen";
import InfoScreen from "../screens/info/InfoScreen";
import UserScreen from "../screens/user/UserScreen";

// HomeStack

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Info: InfoScreen,
    NewMonument: NewMonumentScreen
  },
  {
    initialRouteName: "Home",
    headerMode: "screen"
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    // https://materialdesignicons.com/
    <TabBarIcon name="map-marker" focused={focused} size={26} />
  )
};

// UserStack

const UserStack = createStackNavigator(
  {
    User: UserScreen,
    SingleMonument: SingleMonumentScreen
  },
  {
    initialRouteName: "User",
    headerMode: "screen"
  }
);

UserStack.navigationOptions = {
  tabBarLabel: "User",
  tabBarIcon: ({ focused }) => (
    // https://materialdesignicons.com/
    <TabBarIcon name="account" focused={focused} size={26} />
  )
};

// BottomTabNavigator

export default createMaterialBottomTabNavigator(
  {
    HomeTab: HomeStack,
    UserTab: UserStack
  },
  {
    barStyle: { backgroundColor: colors.darkGreen },
    animatineEnabled: true,
    activeTintColor: colors.gray,
    initialRouteName: "HomeTab",
    shifting: true,
    labeled: false
  }
);
