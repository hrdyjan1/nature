/**
 * @file Actions connected with user
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

// CONSTANTS
// LOCAL IMPORT
import { loginUserAPI, defaultURL } from "../../features/api";

export const LOGOUT = "LOGOUT";
export const LOGIN_SENT = "LOGIN_SENT";
export const LOGIN_FULFILLED = "LOGIN_FULFILLED";
export const LOGIN_REJECTED = "LOGIN_REJECTED";
export const LOGIN_FULFILLED_ADD_MONUMENTS = "LOGIN_FULFILLED_ADD_MONUMENTS";
export const ADD_MONUMENT_ID_TO_USER = "ADD_MONUMENT_ID_TO_USER";
export const ADD_NEW_MONUMENT_TO_USER = "ADD_NEW_MONUMENT_TO_USER";
export const ADD_MONUMENT_REJECTED = "ADD_MONUMENT_REJECTED";
export const ADD_NEW_MONUMENT_TO_USER_FAILED =
  "ADD_NEW_MONUMENT_TO_USER_FAILED";

export function fetchUserName(id) {
  return fetch(defaultURL + "/user", {
    method: "GET",
    headers: { id }
  })
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(error => {
      console.error(error);
    });
}

export const addMonumentIDToUser = monumentID => async dispatch => {
  try {
    dispatch({ type: ADD_MONUMENT_ID_TO_USER, payload: monumentID });
  } catch (error) {
    dispatch({
      type: ADD_NEW_MONUMENT_TO_USER_FAILED,
      payload: "Problem with adding ID to users monuments."
    });
  }
};

export const addMonumentToUser = complexMonument => async dispatch => {
  try {
    const simpleMonument = {
      ...complexMonument,
      category: complexMonument.category._id
    };
    dispatch({ type: ADD_NEW_MONUMENT_TO_USER, payload: simpleMonument });
  } catch (error) {
    dispatch({
      type: ADD_NEW_MONUMENT_TO_USER_FAILED,
      payload: "Problem with adding monument to users monuments."
    });
  }
};

async function _getUsersMonuments(monumentsId) {
  return fetch(`${defaultURL}/CultureMonument`, {
    method: "GET", // or 'PUT'
    headers: {
      "Content-Type": "application/json",
      Monuments: monumentsId
    }
  })
    .then(response => response.json())
    .then(responseJson => responseJson.monuments)
    .catch(error => {
      console.error(error);
    });
}

export const loginUser = (username, password) => async dispatch => {
  // START TO LOGIN
  dispatch({ type: LOGIN_SENT });
  try {
    // TRY TO LOG INTO APP
    const user = await loginUserAPI(username, password);
    // GET MONUMENTS FROM LOGED USER
    const usersMonuments = await _getUsersMonuments(user.user.monumentsId);

    // LOGIN SUCCESS
    dispatch({ type: LOGIN_FULFILLED, payload: user });
    // Fill monuments to logined User
    dispatch({ type: LOGIN_FULFILLED_ADD_MONUMENTS, payload: usersMonuments });
  } catch (error) {
    // LOGIN FAIL
    dispatch({ type: LOGIN_REJECTED, payload: error.message });
  }
};

export const loginUserGoogle = (googleToken, args) => async dispatch => {
  // START TO LOGIN
  dispatch({ type: LOGIN_SENT });
  try {
    // TRY TO LOG INTO APP
    const data = await loginUserAPI(googleToken, args);
    // GET MONUMENTS FROM LOGED USER
    const usersMonuments = await _getUsersMonuments(data.user.monumentsId);

    // LOGIN SUCCESS
    dispatch({ type: LOGIN_FULFILLED, payload: data.user, token: data.token });
    // Fill monuments to logined User
    dispatch({ type: LOGIN_FULFILLED_ADD_MONUMENTS, payload: usersMonuments });
  } catch (error) {
    // LOGIN FAIL
    dispatch({ type: LOGIN_REJECTED, payload: error.message });
  }
  // ziva mapa
};

export const logout = () => ({
  type: LOGOUT
});
