/**
 * @file Actions connected with monument
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

export const DELETE_MONUMENT = "DELETE_MONUMENT";
export const GET_MONUMENTS_SEND = "GET_MONUMENTS_SEND";
export const GET_MONUMENTS_FULFILLED = "GET_MONUMENTS_FULFILLED";
export const GET_MONUMENTS_REJECTED = "GET_MONUMENTS_REJECTED";
export const ADD_MONUMENT_TO_MONUMENTS = "ADD_MONUMENT_TO_MONUMENTS";
export const LIKE_MONUMENT = "LIKE_MONUMENT";

import {
  getAllMonuments,
  deleteMonument as deleteCurrentMonument
} from "../../features/api/monumentApi";

export const getMonuments = () => async dispatch => {
  // START TO GETTING MONUMENTS
  dispatch({ type: GET_MONUMENTS_SEND });
  try {
    // GET MONUMENTS
    const monumentsObject = await getAllMonuments();

    // Throw error if empty of getting problem
    if (!monumentsObject || monumentsObject.error) {
      throw true;
    }

    // GETTING SUCCESS
    dispatch({
      type: GET_MONUMENTS_FULFILLED,
      payload: monumentsObject.monuments
    });
  } catch (error) {
    // GETTING FAIL
    dispatch({
      type: GET_MONUMENTS_REJECTED,
      payload: "Failed to get alll monuments."
    });
  }
};

export const addMonumentToMonuments = monument => ({
  type: ADD_MONUMENT_TO_MONUMENTS,
  payload: monument
});

export const likeMonument = monument => ({
  type: LIKE_MONUMENT,
  payload: monument
});

export const deleteMonument = (monument, userToken) => {
  const info = deleteCurrentMonument(monument, userToken);
  console.log(info);
  return {
    type: DELETE_MONUMENT,
    payload: monument
  };
};
