/**
 * @file Action connected with category
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { defaultURL, addIsChecked } from "../../features/api";

export const FETCH_CATEGORY_BEGIN = "FETCH_CATEGORY_BEGIN";
export const FETCH_CATEGORY_SUCCESS = "FETCH_CATEGORY_SUCCESS";
export const FETCH_CATEGORY_FAILURE = "FETCH_CATEGORY_FAILURE";
export const MAKE_CATEGORY_ISCHECKED = "MAKE_CATEGORY_ISCHECKED";

export function fetchCategory() {
  return dispatch => {
    dispatch(fetchCategoryBegin());
    return fetch(defaultURL + "/CultureCategory")
      .then(handleErrors)
      .then(response => response.json())
      .then(responseJson => {
        dispatch(fetchCategorySuccess(responseJson.CultureCategory));
        return responseJson.CultureCategory;
      })
      .catch(error => dispatch(fetchCategoryFailure(error)));
  };
}

export const fetchCategoryBegin = () => ({
  type: FETCH_CATEGORY_BEGIN
});

export const fetchCategorySuccess = DATA => {
  // New category key isChecked
  addIsChecked(DATA);

  return {
    type: FETCH_CATEGORY_SUCCESS,
    payload: DATA
  };
};

export const fetchCategoryFailure = error => ({
  type: FETCH_CATEGORY_FAILURE,
  payload: { error }
});

export const categoryIsChecked = item => ({
  type: MAKE_CATEGORY_ISCHECKED,
  payload: item
});

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
