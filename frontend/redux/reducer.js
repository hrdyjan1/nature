/**
 * @file Reducer for Redux
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

// Redux
import { combineReducers } from "redux";
import { reducer as i18n } from "react-native-redux-i18n";

// Reducers
import monumentReducer from "./reducer/monumentReducer";

import {
  FETCH_CATEGORY_BEGIN,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_FAILURE,
  MAKE_CATEGORY_ISCHECKED
} from "./action/profileAction";

import {
  LOGIN_SENT,
  LOGIN_FULFILLED,
  LOGIN_REJECTED,
  LOGIN_FULFILLED_ADD_MONUMENTS,
  ADD_MONUMENT_ID_TO_USER,
  ADD_NEW_MONUMENT_TO_USER,
  ADD_NEW_MONUMENT_TO_USER_FAILED,
  LOGOUT
} from "./action/loginAction";
import { LIKE_MONUMENT, DELETE_MONUMENT } from "./action/monumentAction";

const initialCatogoryState = {
  category: [],
  loading: false,
  error: null
};

const initialUserState = {
  user: {},
  loading: false,
  monuments: [],
  logged: false
};

const categoryReducer = (state = initialCatogoryState, action) => {
  let item;
  switch (action.type) {
    case FETCH_CATEGORY_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        category: action.payload
      };

    case FETCH_CATEGORY_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        category: []
      };

    case MAKE_CATEGORY_ISCHECKED:
      item = action.payload;

      state.category.forEach(element => {
        if (element._id === item._id) {
          element.isChecked = item.isChecked;
        }
      });

      return {
        ...state,
        category: [...state.category]
      };

    default:
      return state;
  }
};

const userReducer = (state = initialUserState, action) => {
  switch (action.type) {
    case LOGIN_SENT:
      return {
        ...state,
        loading: true
      };
    case LOGIN_FULFILLED:
      return {
        ...state,
        logged: true,
        user: action.payload,
        token: action.token,
        loading: false
      };
    case LOGIN_REJECTED:
      return {
        ...state,
        loading: false,
        loginError: action.payload
      };
    case LOGIN_FULFILLED_ADD_MONUMENTS:
      return {
        ...state,
        monuments: action.payload
      };
    case ADD_MONUMENT_ID_TO_USER:
      return {
        ...state,
        user: {
          ...state.user,
          monumentsId: [...state.user.monumentsId, action.payload]
        }
      };
    case ADD_NEW_MONUMENT_TO_USER:
      return {
        ...state,
        user: {
          ...state.user
        },
        monuments: [...state.monuments, action.payload]
      };
    // case ADD_NEW_MONUMENT_TO_USER_FAILED:
    //   return {
    //     ...state,
    //     error: action.payload
    //   };
    case ADD_NEW_MONUMENT_TO_USER_FAILED:
      return {
        ...state,
        error: action.payload
      };
    case DELETE_MONUMENT:
      return {
        ...state,
        user: {
          ...state.user,
          monumentsId:
            state.user.monumentsId &&
            state.user.monumentsId.filter(id => id !== action.payload._id)
        },
        monuments:
          state.monuments &&
          state.monuments.filter(
            monument => monument._id !== action.payload._id
          )
      };
    case LIKE_MONUMENT:
      return {
        ...state,
        monuments:
          state.monuments &&
          state.monuments.map(monument =>
            monument._id === action.payload._id
              ? { ...monument, likes: monument.likes + 1 }
              : monument
          )
      };
    case LOGOUT: {
      return { ...initialUserState };
    }
    default:
      return state;
  }
};

const reducer = combineReducers({
  i18n,
  monument: monumentReducer,
  category: categoryReducer,
  user: userReducer
});

export default reducer;
