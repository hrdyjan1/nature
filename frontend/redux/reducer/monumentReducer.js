/**
 * @file Reducer only of monument operations
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

// Actions
import {
  GET_MONUMENTS_SEND,
  GET_MONUMENTS_FULFILLED,
  GET_MONUMENTS_REJECTED,
  ADD_MONUMENT_TO_MONUMENTS,
  LIKE_MONUMENT,
  DELETE_MONUMENT
} from "../action/monumentAction";

// InititalState
const initialMonumentState = {
  list: [],
  loading: false,
  error: { isError: false, errorMessage: null }
};

// Main reducer
const monumentReducer = (state = initialMonumentState, action) => {
  switch (action.type) {
    case LIKE_MONUMENT:
      // console.log(action.payload);
      return {
        ...state,
        list: state.list.map(monument =>
          monument._id === action.payload._id
            ? { ...monument, likes: monument.likes + 1 }
            : monument
        )
      };
    case GET_MONUMENTS_SEND:
      return {
        ...state,
        loading: true
      };
    case GET_MONUMENTS_FULFILLED:
      return {
        ...state,
        list: action.payload,
        error: { isError: false, errorMessage: null },
        loading: false
      };
    case GET_MONUMENTS_REJECTED:
      return {
        ...state,
        loading: false,
        error: { isError: true, errorMessage: action.payload }
      };
    case ADD_MONUMENT_TO_MONUMENTS:
      return {
        ...state,
        list: [...state.list, action.payload]
      };
    case DELETE_MONUMENT:
      return {
        ...state,
        list: state.list.filter(monument => monument._id !== action.payload._id)
      };
    default:
      return state;
  }
};

export default monumentReducer;
