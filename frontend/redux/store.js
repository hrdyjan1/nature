/**
 * @file Store for Redux
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { createStore, applyMiddleware } from "redux";
import { setLocale } from "../features/I18n";

import reducer from "./reducer";

const thunk = store => next => action => {
  if (typeof action === "function") {
    action(store.dispatch);
  } else {
    next(action);
  }
};

const store = createStore(reducer, applyMiddleware(thunk));

store.dispatch(setLocale("en"));

export default store;
