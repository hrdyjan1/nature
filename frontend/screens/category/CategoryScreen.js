/**
 * @file Screen for showing all categories
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View, Text } from "react-native";
import LoadingScreen from "../loading/LoadingScreen";

import { connect } from "react-redux";
import ListOfSearchItems from "../../componets/listOFItems/ListOfSearchItems";

import colors from "../../features/colors";

import { SECONDSCREENAME } from "../../features/constats";
import HeaderButton from "../../componets/buttons/HeaderButton";

class CategoryScreen extends Component {
  state = {
    numOfColumns: 2
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: SECONDSCREENAME,
      headerRight: (
        <HeaderButton
          name="plus"
          fontType="material-community"
          color={colors.darkGreen}
          method={() => navigation.navigate("NewMonument")}
        />
      )
    };
  };

  render() {
    const { category } = this.props;

    if (category.loading) {
      return <LoadingScreen />;
    } else if (category.error) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <Text>{category.error}</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 1, marginTop: 20 }}>
          <ListOfSearchItems
            data={category.category}
            navigation={this.props.navigation}
            numberOfColumens={this.state.numOfColumns}
          />
        </View>
      );
    }
  }
}

const mapStateToProps = state => ({
  category: state.category
});

export default connect(mapStateToProps)(CategoryScreen);
