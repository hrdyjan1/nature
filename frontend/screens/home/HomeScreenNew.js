/**
 * @file Screen with map
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View, Alert, Text } from "react-native";
import Expo from "expo";
import MapView, { Marker } from "react-native-maps";

// Redux
import { connect } from "react-redux";
import { fetchCategory } from "../../redux/action/profileAction";
import { likeMonument } from "../../redux/action/monumentAction";

// Features
import i18n from "../../features/I18n";

// Components
import OdinaryButton from "../../componets/buttons/OdinaryButton";
import BasicPage from "../../componets/basic/BasicPage";
import styles from "../../componets/basic/style";
import Markers from "../../componets/basic/Markers";

class HomeScreenNew extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      // Header
      leftIcon: "menu",
      leftIconMethod: this.moveToScreenInfo,
      rightIcon: "map-marker-plus",
      rightIconMethod: this.moveToScreenAddMonument,
      // Own location
      location: { coords: null },
      // General region
      region: null,
      isCurrentRegionShowned: false,
      isLoadingCurrentRegion: false
    };
  }

  moveToScreenInfo = () => {
    this.props.navigation.navigate("Info");
  };

  moveToScreenAddMonument = () => {
    if (this.state.location === null) {
      alert(i18n.t("home.locationLoading"));
    } else if (!this.props.user.logged) {
      alert(i18n.t("basic.loginRequier"));
    } else {
      this.props.navigation.navigate("NewMonument", {
        coords: this.state.location.coords
      });
    }
  };

  setOwnLocation = async () => {
    let { status } = await Expo.Permissions.askAsync(Expo.Permissions.LOCATION);
    if (status !== "granted") {
      alert(i18n.t("home.locationPermission"));
      return;
    }
    let location = await Expo.Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  setCurrentRegion = async () => {
    const location = await Expo.Location.getCurrentPositionAsync({});
    this.setState({
      region: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      isCurrentRegionShowned: true,
      isLoadingCurrentRegion: false
    });
  };

  showCurrentRegion = () => {
    this.setState({ isLoadingCurrentRegion: true });
    this.setCurrentRegion();
  };

  onRegionChange(region) {
    this.setState({ isCurrentRegionShowned: false });
    this.setState({ region });
  }

  handleMapPress = ({ coordinate }) => {
    Alert.alert(
      i18n.t("home.newMonumentHeading"),
      i18n.t("home.newMonumentText"),
      [
        {
          text: i18n.t("basic.no"),
          style: "cancel"
        },
        {
          text: i18n.t("basic.yes"),
          onPress: () =>
            this.props.navigation.navigate("NewMonument", {
              coords: coordinate
            })
        }
      ]
    );
  };

  componentDidMount() {
    this.props.fetchCategory();
    this.setOwnLocation();
  }

  // componentDidUpdate(prevProps) {
  //   const newList = this.props.monument.list;
  //   const oldList = prevProps.monument.list;

  //   console.log("Component did update in HomeScreenNew");
  //   console.log(newList);
  //   console.log(oldList);
  //
  //   const isSame =
  //     newList.length === oldList.length &&
  //     newList.every(
  //       (o, i) =>
  //         Object.keys(o).length === Object.keys(oldList[i]).length &&
  //         Object.keys(o).every(k => o[k] === oldList[i][k])
  //     );
  //   if (!isSame) {
  //     console.log("different");
  //   } else {
  //     console.log("same");
  //   }
  // }

  render() {
    // Declaration
    const {
      leftIcon,
      leftIconMethod,
      rightIcon,
      rightIconMethod,
      location
    } = this.state;

    // Check if should render current region
    const currentRegion = this.state.isCurrentRegionShowned
      ? this.state.region
      : undefined;

    return (
      <BasicPage
        icons={{ leftIcon, rightIcon }}
        methods={{ leftIconMethod, rightIconMethod }}
        navigation={this.props.navigation}
        loading={
          !location.coords ||
          (this.props.monument && this.props.monument.loading)
        }
      >
        {location.coords && (
          <View style={styles.container}>
            <MapView
              style={styles.mapView}
              region={currentRegion}
              onLongPress={e => this.handleMapPress(e.nativeEvent)}
              onRegionChange={region => this.onRegionChange(region)}
              initialRegion={{
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
              }}
            >
              {/* <Marker coordinate={location.coords} title="You are here!" /> */}
              <Markers
                likeSingleMonument={this.props.likeSingleMonument}
                navigate={this.props.navigation.navigate}
                rootMonument={this.props.monument}
                rootCategory={this.props.category}
              />
            </MapView>
            <OdinaryButton
              name={i18n.t("home.possition")}
              method={this.showCurrentRegion}
              loading={this.state.isLoadingCurrentRegion}
            />
          </View>
        )}
      </BasicPage>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    likeSingleMonument: monument => {
      dispatch(likeMonument(monument));
      Alert.alert(i18n.t("home.likeHeading"), i18n.t("home.likeText"));
    },
    fetchCategory: () => {
      dispatch(fetchCategory());
    }
  };
};

const mapStateToProps = state => ({
  monument: state.monument,
  category: state.category,
  i18n: state.i18n,
  user: state.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreenNew);
