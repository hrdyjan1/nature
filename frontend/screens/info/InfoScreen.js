/**
 * @file Screen with diffrerent categories and languages change
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import * as RNE from "react-native-elements";
import { View, ScrollView, Text } from "react-native";

// Redux
import { connect } from "react-redux";
import { categoryIsChecked } from "../../redux/action/profileAction";

// Features
import styles from "./style";
import colors from "../../features/colors";
import i18n, { setLocale, Loc } from "../../features/I18n";

// Components
import ListOfFilters from "./ListOfFilters";
import HeaderButton from "../../componets/buttons/HeaderButton";
import withListOf from "../../componets/listOFItems/withListOf";
import OdinaryButton from "../../componets/buttons/OdinaryButton";

// Constants
const Filters = withListOf("category")(ListOfFilters);

// Screen
class InfoFilterScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const informations = i18n.t("info.informations");
    return {
      headerTitle: informations,
      headerLeft: (
        <HeaderButton
          name="arrow-left"
          color={colors.darkGreen}
          method={() => navigation.goBack()}
        />
      )
    };
  };

  switchCategoryIsChecked = item => {
    const newItem = {
      ...item,
      isChecked: !item.isChecked
    };

    this.props.categoryIsChecked(newItem);
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.heading}>
          <RNE.Text h2 style={styles.headingText}>
            <Loc locKey="info.filters" />
          </RNE.Text>
        </View>
        <Filters
          {...this.props}
          switchCategoryIsChecked={this.switchCategoryIsChecked}
        />
        <View style={styles.heading}>
          <RNE.Text h2 style={styles.headingText}>
            <Loc locKey="info.languages" />
          </RNE.Text>
        </View>

        <OdinaryButton
          name={i18n.t("info.language_en")}
          method={() => {
            this.props.changeLanguage("en");
            this.props.navigation.setParams({
              refresh: true
            });
          }}
        />
        <OdinaryButton
          name={i18n.t("info.language_cs")}
          method={() => {
            this.props.changeLanguage("cs");
            this.props.navigation.setParams({
              refresh: true
            });
          }}
        />
      </ScrollView>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeLanguage: locale => {
      dispatch(setLocale(locale));
    },
    categoryIsChecked: newItem => {
      dispatch(categoryIsChecked(newItem));
    }
  };
};

const mapStateToProps = state => ({
  i18n: state.i18n,
  category: state.category,
  user: state.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoFilterScreen);
