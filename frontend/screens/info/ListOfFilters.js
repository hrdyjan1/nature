/**
 * @file Component for showing categories
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { ListItem, List } from "react-native-elements";

import TabBarIcon from "../../componets/navigation/TabBarIcon";
import colors from "../../features/colors";

const ListOfFilters = ({ list, switchCategoryIsChecked }) => {
  // console.log(switchCategoryIsChecked);
  return (
    <List>
      {list.map(item => (
        <ListItem
          onPress={() => switchCategoryIsChecked(item)}
          key={item._id}
          title={item.title}
          subtitle={item.description}
          leftIcon={
            <TabBarIcon
              name={`checkbox-marked${item.isChecked ? "" : "-outline"}`}
              defaultColor={item.color || colors.darkGreen}
              size={26}
            />
          }
          rightIcon={
            <TabBarIcon
              name="map-marker"
              defaultColor={item.color || colors.darkGreen}
              size={26}
            />
          }
        />
      ))}
    </List>
  );
};

export default ListOfFilters;
