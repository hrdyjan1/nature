/**
 * @file Styles for info screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";

export default (styles = StyleSheet.create({
  heading: {
    alignItems: "center",
    justifyContent: "center"
  },
  headingText: {
    textAlign: "center"
  }
}));
