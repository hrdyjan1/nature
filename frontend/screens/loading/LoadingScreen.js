/**
 * @file Screen prepared for loading
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { View } from "react-native";
import SpriteSheet from "rn-sprite-sheet";

class LoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loop: true,
      resetAfterFinish: true,
      fps: 22
    };
  }

  async componentDidMount() {
    this.loadingImage.play({
      type: "walk",
      fps: this.state.fps,
      loop: this.state.loop,
      resetAfterFinish: this.state.resetAfterFinish
    });
  }

  componentWillUnMount = () => {
    this.loadingImage.stop(() => console.log("stopped"));
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <SpriteSheet
            ref={ref => (this.loadingImage = ref)}
            source={require("../../assets/loading.png")}
            columns={30}
            rows={1}
            width={120}
            imageStyle={{ marginTop: -1 }}
            animations={{
              walk: Array.from({ length: 30 }, (v, i) => i)
            }}
          />
        </View>
      </View>
    );
  }
}

export default LoadingScreen;
