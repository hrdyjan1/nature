/**
 * @file Screen with sign up, sign in and anonymous entrance
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View, ImageBackground } from "react-native";
import { Button, Text } from "react-native-elements";

// Redux
import { connect } from "react-redux";
import { getMonuments } from "../../redux/action/monumentAction";

// Features
import i18n from "../../features/I18n";
import styles from "../../styles/LoginStyle";

// Components
import BasicPage from "../../componets/basic/BasicPage";

class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      leftIcon: null,
      leftIconMethod: null,
      rightIcon: null,
      rightIconMethod: null
    };
  }

  _signIn = () => {
    this.props.navigation.navigate("SignIn");
  };

  _signUp = () => {
    this.props.navigation.navigate("SignUp");
  };

  _anonymous = () => {
    this.props.navigation.navigate("Home");
  };

  componentDidMount() {
    this.props.dispatch(getMonuments());
  }

  render() {
    const { leftIcon, leftIconMethod, rightIcon, rightIconMethod } = this.state;
    return (
      <BasicPage
        navigation={this.props.navigation}
        icons={{ leftIcon, rightIcon }}
        methods={{ leftIconMethod, rightIconMethod }}
      >
        <View style={styles.container}>
          <ImageBackground
            source={require("../../assets/background.jpg")}
            style={styles.backgroundImage}
            resizeMode="cover"
          >
            <View style={styles.top}>
              <Text h1 style={styles.headingText}>
                {i18n.t("basic.wellcome")}
              </Text>
            </View>

            <View style={styles.bottom}>
              <Button
                raised
                title={i18n.t("basic.signUp")}
                onPress={this._signUp}
                buttonStyle={[styles.button, styles.buttonSignUp]}
                icon={{ name: "account-plus", type: "material-community" }}
              />
              <Button
                raised
                title={i18n.t("basic.signIn")}
                onPress={this._signIn}
                buttonStyle={[styles.button, styles.buttonSignIn]}
                icon={{ name: "account-check", type: "material-community" }}
              />
              <Button
                raised
                title={i18n.t("basic.anonymous")}
                onPress={this._anonymous}
                buttonStyle={[styles.button, styles.buttonAnonymous]}
                icon={{ name: "account-multiple", type: "material-community" }}
              />
            </View>
          </ImageBackground>
        </View>
      </BasicPage>
    );
  }
}
const mapStateToProps = state => ({
  i18n: state.i18n
});

export default connect(mapStateToProps)(LoginScreen);
