/**
 * @file Screen for sign in
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View } from "react-native";

import Expo from "expo";
import {
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from "react-native-elements";

// Redux
import { connect } from "react-redux";
import { loginUser } from "../../redux/action";

// Features
import i18n from "../../features/I18n";
import styles from "./style";
import colors from "../../features/colors";
import { GOOGLEINFO } from "../../features/constats";
import { loginUserGoogle } from "../../redux/action/loginAction";
import LoadingScreen from "../loading/LoadingScreen";

// COMPONENT
class SignInScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: i18n.t("basic.signIn"),
      headerLeft: (
        <Button
          icon={{
            name: "arrow-left",
            size: 24,
            type: "material-community",
            color: colors.darkGreen
          }}
          onPress={() => navigation.goBack()}
          backgroundColor="transparent"
        />
      )
    };
  };
  state = {
    email: "",
    password: "",
    error: ""
  };

  componentDidUpdate() {
    const { user } = this.props;
    if (user.token) {
      const { navigate } = this.props.navigation;
      navigate("Home");
    }
  }

  _changeEmail = email => {
    this.setState({ email });
  };

  _changePassword = password => {
    this.setState({ password });
  };

  _signInWithGoogleAsync = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: GOOGLEINFO.ANDROIDID,
        iosClientId: GOOGLEINFO.IOSID,
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        this.props.dispatch(loginUserGoogle(result.accessToken));
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };

  _signIn = async () => {
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Require every field to be filled." });
    } else {
      await this.props.loginUserGoogle({
        email: this.state.email,
        password: this.state.password
      });
    }
  };

  componentDidUpdate() {
    const { user } = this.props;
    if (user && user.token) {
      const { navigate } = this.props.navigation;
      navigate("Home");
    }
  }

  render() {
    const { user } = this.props;

    if (user && user.loading) {
      return (
        <Expo.LinearGradient
          style={styles.container}
          colors={[colors.lightBlue, colors.white]}
          start={[0, 0]}
          end={[0, 0.8]}
        >
          <LoadingScreen style={styles.container} />;
        </Expo.LinearGradient>
      );
    }

    return (
      <Expo.LinearGradient
        style={styles.container}
        colors={[colors.lightBlue, colors.white]}
        start={[0, 0]}
        end={[0, 0.8]}
      >
        <View style={styles.top}>
          <Button
            raised
            title={i18n.t("basic.googleConnection")}
            onPress={this._signInWithGoogleAsync}
            buttonStyle={[styles.button, styles.buttonConnectWithGoogle]}
            icon={{ name: "google", type: "material-community" }}
          />
        </View>

        <View style={styles.middle}>
          <FormLabel>{i18n.t("basic.email")}</FormLabel>
          <FormInput onChangeText={this._changeEmail} />

          <FormLabel>{i18n.t("basic.password")}</FormLabel>
          <FormInput onChangeText={this._changePassword} />

          <FormValidationMessage style={{ alignItems: "center" }}>
            {this.state.error || (user && user.loginError)}
          </FormValidationMessage>
        </View>

        <View style={styles.bottom}>
          <Button
            raised
            title={i18n.t("basic.goBack")}
            onPress={this._goBack}
            buttonStyle={[styles.button, styles.buttonAnonymous]}
            icon={{ name: "arrow-left", type: "material-community" }}
          />
          <Button
            raised
            title={i18n.t("basic.signUp")}
            onPress={this._signIn}
            buttonStyle={[styles.button, styles.buttonSignIn]}
            icon={{ name: "account-plus", type: "material-community" }}
          />
        </View>
      </Expo.LinearGradient>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginUserGoogle: user => {
      dispatch(loginUserGoogle(null, user));
    }
  };
};

const mapStateToProps = state => ({
  user: state.user,
  i18n: state.i18n
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInScreen);
