/**
 * @file Screen for sign up
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View } from "react-native";
import Expo from "expo";
import {
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from "react-native-elements";

// Redux
import { connect } from "react-redux";

// Features
import i18n from "../../features/I18n";
import styles from "../../styles/SignUpStyle";
import LoadingScreen from "../loading/LoadingScreen";
import { GOOGLEINFO } from "../../features/constats";
import { loginUserGoogle } from "../../redux/action/loginAction";
import colors from "../../features/colors";

class SignUpScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: i18n.t("basic.signUp"),
      headerLeft: (
        <Button
          icon={{
            name: "arrow-left",
            size: 24,
            type: "material-community",
            color: colors.darkGreen
          }}
          onPress={() => navigation.goBack()}
          backgroundColor="transparent"
        />
      )
    };
  };

  state = {
    error: "",
    firstName: "",
    lastName: "",
    email: "",
    password: ""
  };

  componentDidUpdate() {
    const { user } = this.props;
    if (user.token) {
      const { navigate } = this.props.navigation;
      navigate("Home");
    }
  }

  // HANDLERS

  _handleChangeFirstName = firstName => {
    this.setState({ firstName });
  };

  _handleChangeLastName = lastName => {
    this.setState({ lastName });
  };

  _handleChangeEmail = email => {
    this.setState({ email });
  };
  _handleChangePassword = password => {
    this.setState({ password });
  };

  // OTHER METHODS

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _signUp = async () => {
    const { firstName, lastName, email, password } = this.state;
    if (!firstName || !lastName || !email || !password) {
      this.setState({ error: "Require every field to be filled." });
    } else {
      await this.props.dispatch(
        loginUserGoogle(null, {
          firstName,
          lastName,
          email,
          password
        })
      );
    }
  };

  _signInWithGoogleAsync = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: GOOGLEINFO.ANDROIDID,
        iosClientId: GOOGLEINFO.IOSID,
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        this.props.dispatch(loginUserGoogle(result.accessToken));
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };

  render() {
    const { user } = this.props;

    // console.log("-----------------user-------------------");
    // console.log(user);

    if (user && user.loading) {
      return (
        <Expo.LinearGradient
          style={styles.container}
          colors={[colors.lightBlue, colors.white]}
          start={[0, 0]}
          end={[0, 0.8]}
        >
          <LoadingScreen style={styles.container} />;
        </Expo.LinearGradient>
      );
    }

    return (
      <Expo.LinearGradient
        style={styles.container}
        colors={[colors.lightBlue, colors.white]}
        start={[0, 0]}
        end={[0, 0.8]}
      >
        <View style={styles.top}>
          <Button
            raised
            title={i18n.t("basic.googleConnection")}
            onPress={this._signInWithGoogleAsync}
            buttonStyle={[styles.button, styles.buttonConnectWithGoogle]}
            icon={{ name: "google", type: "material-community" }}
          />
        </View>

        <View style={styles.middle}>
          <FormLabel>{i18n.t("basic.firstName")}</FormLabel>
          <FormInput onChangeText={this._handleChangeFirstName} />

          <FormLabel>{i18n.t("basic.lastName")}</FormLabel>
          <FormInput onChangeText={this._handleChangeLastName} />

          <FormLabel>{i18n.t("basic.email")}</FormLabel>
          <FormInput onChangeText={this._handleChangeEmail} />

          <FormLabel>{i18n.t("basic.password")}</FormLabel>
          <FormInput onChangeText={this._handleChangePassword} />

          <FormValidationMessage style={{ alignItems: "center" }}>
            {this.state.error || (user && user.loginError)}
          </FormValidationMessage>
        </View>

        <View style={styles.bottom}>
          <Button
            raised
            title={i18n.t("basic.goBack")}
            onPress={this._goBack}
            buttonStyle={[styles.button, styles.buttonAnonymous]}
            icon={{ name: "arrow-left", type: "material-community" }}
          />
          <Button
            raised
            title={i18n.t("basic.signUp")}
            onPress={this._signUp}
            buttonStyle={[styles.button, styles.buttonSignUp]}
            icon={{ name: "account-plus", type: "material-community" }}
          />
        </View>
      </Expo.LinearGradient>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  i18n: state.i18n
});

export default connect(mapStateToProps)(SignUpScreen);
