/**
 * @file Screen showing all monuments from chosen category
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { FlatList } from "react-native";

import { List, ListItem } from "react-native-elements";

import { getAllCultureMonumentFromID } from "../../features/api";
import LoadingScreen from "../loading/LoadingScreen";

export default class ListOfMonumentScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const category = navigation.getParam("category", null);
    return {
      headerTitle: category.title || ""
    };
  };

  state = {
    categoryID: "",
    dataSource: [],
    categoryTitle: ""
  };

  async componentDidMount() {
    const currentCategory = this.props.navigation.getParam("category", null);

    const currentCategoryID = currentCategory._id;
    const currentCategoryTitle = currentCategory.title;

    this.setState({
      categoryID: currentCategoryID,
      categoryTitle: currentCategoryTitle
    });
    getAllCultureMonumentFromID(currentCategoryID)
      .then(arrOfMonuments => {
        this.setState({ dataSource: arrOfMonuments });
      })
      .catch(error => {
        console.error(error);
      });
  }

  returnPlainRow = ({ item }) => {
    const { categoryTitle } = this.state;

    return (
      <ListItem
        title={item.title}
        hideChevron={false}
        onPress={() =>
          this.props.navigation.navigate("SingleMonument", {
            item,
            categoryTitle
          })
        }
      />
    );
  };

  render() {
    const { categoryID, dataSource, categoryTitle } = this.state;

    if (categoryID.length === 0 || !dataSource || !categoryTitle) {
      return <LoadingScreen />;
    }

    return (
      <List>
        <FlatList
          data={dataSource}
          renderItem={this.returnPlainRow}
          keyExtractor={({ _id }, index) => _id}
        />
      </List>
    );
  }
}
