/**
 * @file Screen for creating new monument
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { Keyboard } from "react-native";
import * as RNE from "react-native-elements";

import { connect } from "react-redux";
import { addMonumentIDToUser, addMonumentToUser } from "../../redux/action";
import { addMonumentToMonuments } from "../../redux/action";

// Feature
import { createMonument } from "../../features/api";
import i18n from "../../features/I18n";

// Constatns
import colors from "../../features/colors";
import BasicPage from "../../componets/basic/BasicPage";

class NewMonumentScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      // Basic Header
      isLogginRequire: true,
      isUserLogged: this.props.user.user ? true : false,
      leftIcon: "arrow-left",
      leftIconMethod: () => this.props.navigation.goBack(),
      //Error
      error: "",
      // Monument
      latitude: null,
      longitude: null,
      titleOfMonument: "",
      descriptionOfMonument: "",
      authorOfMonumentName:
        (this.props.user.user && this.props.user.user.name) || "Anonymous",
      authorOfMonumentID:
        (this.props.user.user && this.props.user.user._id) || "123456",
      category: [],
      // Monument end
      loadingSendForm: false,
      selectedCategory: 0
    };
  }

  // Set Category by Navigation props

  componentDidMount() {
    const { category } = this.props.category;
    let coords = this.props.navigation.getParam("coords");

    const categoryFromProps = category.map(item => {
      const { _id, title } = item;
      return { _id, title };
    });

    this.setState({
      category: categoryFromProps,
      latitude: coords.latitude,
      longitude: coords.longitude
    });
  }

  // Handlers

  _handleChangeTitle = titleOfMonument => {
    this.setState({ titleOfMonument });
  };

  _handleChangeDescription = descriptionOfMonument => {
    this.setState({ descriptionOfMonument });
  };

  _handleSelectCategory = selectedCategory => {
    this.setState({ selectedCategory });
  };

  _handleSubmit = async () => {
    Keyboard.dismiss();
    this.setState({ loadingSendForm: true });

    let createdMonument = null;

    if (this.state.titleOfMonument && this.state.descriptionOfMonument) {
      const newMonument = {
        title: this.state.titleOfMonument,
        description: this.state.descriptionOfMonument,
        author: this.state.authorOfMonumentName,
        authorID: this.state.authorOfMonumentID,
        likes: 0,
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        categoryID: this.state.category[this.state.selectedCategory]._id
      };

      createdMonument = await this._saveNewMonumentToDatabase(newMonument);
    }

    if (createdMonument && createdMonument.CultureMonuments) {
      await this.props.dispatch(
        addMonumentIDToUser(createdMonument.CultureMonuments._id)
      );

      await this.props.dispatch(
        addMonumentToUser(createdMonument.CultureMonuments)
      );

      await this.props.dispatch(
        addMonumentToMonuments(createdMonument.CultureMonuments)
      );

      this.setState({
        loadingSendForm: false
      });
      this.props.navigation.goBack();
    } else if (createdMonument && createdMonument.error) {
      this.setState({
        error: createdMonument.message,
        loadingSendForm: false
      });
    } else {
      this.setState({
        error: "Every input field is required.",
        loadingSendForm: false
      });
    }
  };

  // Hhandlers END

  // Other methods

  _saveNewMonumentToDatabase = async newMonument => {
    return createMonument(newMonument, this.props.user.token)
      .then(response => response.json())
      .catch(error => {
        console.error(error);
      });
  };

  // Other Methods END

  // RENDER

  render() {
    const categoryItems = this.state.category.map(item => item.title);
    const {
      selectedCategory,
      loadingSendForm,
      error,
      isLogginRequire,
      isUserLogged,
      // Icons
      leftIcon,
      rightIconMethod,
      rightIcon,
      leftIconMethod
    } = this.state;

    return (
      <BasicPage
        isLogginRequire={isLogginRequire}
        isUserLogged={isUserLogged}
        icons={{ leftIcon, rightIcon }}
        methods={{ leftIconMethod, rightIconMethod }}
        navigation={this.props.navigation}
        loading={
          this.props.category.isLoading || this.state.category.length === 0
        }
      >
        <RNE.FormLabel>{i18n.t("monument.title")}</RNE.FormLabel>
        <RNE.FormInput
          onChangeText={this._handleChangeTitle}
          value={this.state.titleOfMonument}
        />
        <RNE.FormLabel>{i18n.t("monument.description")}</RNE.FormLabel>
        <RNE.FormInput
          onChangeText={this._handleChangeDescription}
          value={this.state.descriptionOfMonument}
        />

        <RNE.ButtonGroup
          onPress={this._handleSelectCategory}
          selectedIndex={selectedCategory}
          buttons={categoryItems}
          containerStyle={{ height: 100 }}
        />
        <RNE.Button
          disabled={loadingSendForm}
          loading={loadingSendForm}
          onPress={this._handleSubmit}
          title={i18n.t("monument.addMonument")}
          loadingProps={{ size: "large", color: "rgba(111, 202, 186, 1)" }}
          titleStyle={{ fontWeight: "700" }}
          buttonStyle={{
            backgroundColor: colors.mainColor,
            width: 300,
            height: 45,
            borderColor: "transparent",
            borderWidth: 0,
            borderRadius: 5
          }}
          containerStyle={{ marginTop: 20 }}
        />
        <RNE.FormValidationMessage>{error}</RNE.FormValidationMessage>
      </BasicPage>
    );
  }
}

const mapStateToProps = state => ({
  i18n: state.i18n,
  category: state.category,
  user: state.user
});

export default connect(mapStateToProps)(NewMonumentScreen);
