/**
 * @file Screen showing one monument plus info
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { ScrollView, Alert } from "react-native";
import * as RNE from "react-native-elements";

// Redux
import { connect } from "react-redux";
import { fetchUserName } from "../../redux/action/loginAction";
import { deleteMonument } from "../../redux/action/monumentAction";

// Features
import i18n from "../../features/I18n";

// Components
import ListItem from "../user/ListItem";
import BasicPage from "../../componets/basic/BasicPage";
import OdinaryButton from "../../componets/buttons/OdinaryButton";

class SingleMonumentScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      leftIcon: "arrow-left",
      leftIconMethod: () => this.props.navigation.goBack(),
      monument: {}
    };
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const { category } = this.props.category;
    let monument = navigation.getParam("monument");
    // console.log(monument);

    // if (!monument.author) {
    //   await fetchUserName(monument.authorID)
    //     .then(authorID => {
    //       monument.author = authorID.user.name;
    //     })
    //     .catch(error => {
    //       console.error(error);
    //     });
    // }

    const { title } =
      category.find(singleCategory => {
        return (
          singleCategory._id === monument.category ||
          singleCategory._id === monument.category._id
        );
      }) || undefined;

    monument.categoryName = title;

    this.setState({ monument });
  }

  removeMonument = () => {
    Alert.alert(
      i18n.t("monument.removeHeading"),
      i18n.t("monument.removeText"),
      [
        {
          text: i18n.t("basic.no"),
          style: "cancel"
        },
        {
          text: i18n.t("basic.yes"),
          onPress: () => {
            this.props.deleteMonument(
              this.state.monument,
              this.props.user.token
            );
            this.props.navigation.navigate("User");
          }
        }
      ]
    );
  };

  render() {
    console.log("this.state.monument");
    console.log(this.state.monument);
    const {
      monument,
      leftIcon,
      leftIconMethod,
      rightIcon,
      rightIconMethod
    } = this.state;
    return (
      <BasicPage
        // isUserLogged={this.props.user}
        icons={{ leftIcon, rightIcon }}
        methods={{ leftIconMethod, rightIconMethod }}
        navigation={this.props.navigation}
        loading={monument == undefined}
      >
        <ScrollView>
          <RNE.List>
            <ListItem
              title={i18n.t("monument.title")}
              subtitle={monument.title}
              hideChevron={true}
            />
            <ListItem
              title={i18n.t("monument.description")}
              subtitle={monument.description}
              hideChevron={true}
            />
            <ListItem
              title={i18n.t("monument.author")}
              subtitle={monument.author}
              hideChevron={true}
            />
            <ListItem
              title={i18n.t("monument.category")}
              subtitle={monument.categoryName}
              hideChevron={true}
            />
            <ListItem
              title={i18n.t("monument.likes")}
              subtitle={monument.likes}
              hideChevron={true}
            />
          </RNE.List>
          {this.state.monument.authorID === this.props.user.user._id ? (
            <OdinaryButton
              name="Remove monument"
              method={this.removeMonument}
            />
          ) : null}
        </ScrollView>
      </BasicPage>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    deleteMonument: (monument, userToken) => {
      dispatch(deleteMonument(monument, userToken));
    }
  };
};

const mapStateToProps = state => ({
  user: state.user,
  i18n: state.i18n,
  category: state.category
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleMonumentScreen);
