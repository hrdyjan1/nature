/**
 * @file Component for one line of monument
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import { Text, View } from "react-native";
import * as RNE from "react-native-elements";

// Features
import styles from "./style";

const ListItem = ({ title, subtitle, onPress, hideChevron }) => (
  <RNE.ListItem
    hideChevron={hideChevron}
    onPress={onPress}
    title={
      <View style={styles.titleView}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
    }
    subtitle={
      <View style={styles.subtitleView}>
        <Text style={styles.subtitleText}>{subtitle}</Text>
      </View>
    }
  />
);

export default ListItem;
