/**
 * @file Component for showing lines of monuments
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React from "react";
import ListItem from "./ListItem";

const ListOfMonuments = ({ monuments, navigation, author }) => {
  if (!monuments) {
    return null;
  }
  return (
    monuments &&
    monuments.map(monument => (
      <ListItem
        onPress={() =>
          navigation.navigate("SingleMonument", {
            monument: { ...monument, author }
          })
        }
        key={monument._id}
        title={monument.title}
        subtitle={monument.description}
      />
    ))
  );
};

export default ListOfMonuments;
