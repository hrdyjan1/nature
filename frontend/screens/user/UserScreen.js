/**
 * @file Screen with information about user
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import * as RNE from "react-native-elements";

// Redux
import { connect } from "react-redux";
import { logout } from "../../redux/action/loginAction";

// Components
import BasicPage from "../../componets/basic/BasicPage";
import ListItem from "./ListItem";

// Features
import styles from "./style";
import ListOfMonuments from "./ListOfMonuments";
import i18n from "../../features/I18n";

function comparer(otherArray) {
  return function(current) {
    return (
      otherArray.filter(function(other) {
        return other.value == current.value && other.display == current.display;
      }).length == 0
    );
  };
}

class UserScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      isLogginRequire: true,
      isUserLogged: this.props.user.logged,
      leftIcon: null,
      leftIconMethod: null,
      rightIcon: this.props.user.logged ? "logout" : null,
      rightIconMethod: this.props.logout,
      user: {},
      monuments: []
    };
  }

  componentDidUpdate(prevProps) {
    // console.log("Component Did Update");
    // console.log(this.props.user);

    const newUser = this.props.user || null;
    const oldUser = prevProps.user || null;

    if (!newUser || !oldUser) {
      return;
    } else if (newUser.logged !== oldUser.logged) {
      this.setState({
        isUserLogged: newUser.logged,
        user: newUser.user,
        monuments: newUser.monuments
      });
      alert(i18n.t("basic.logoutText"));
      return;
    } else if (!newUser.user || !oldUser.user) {
      return;
    } else if (!newUser.user.monumentsId || !oldUser.user.monumentsId) {
      return;
    } else {
      if (newUser.user.monumentsId.length !== oldUser.user.monumentsId.length) {
        this.setState(prevState => ({
          ...prevState,
          user: { ...prevState.user, monumentsId: newUser.user.monumentsId }
        }));
      }
      if (newUser.monuments.length !== oldUser.monuments.length) {
        this.setState({ monuments: newUser.monuments });
      }
      const isSame =
        newUser.monuments.length === oldUser.monuments.length &&
        newUser.monuments.every(
          (o, i) =>
            Object.keys(o).length ===
              Object.keys(oldUser.monuments[i]).length &&
            Object.keys(o).every(k => o[k] === oldUser.monuments[i][k])
        );
      if (!isSame) {
        this.setState({ monuments: newUser.monuments });
      }
    }
  }

  componentDidMount() {
    const user = this.props.user;
    this.setState({ user: user.user || {} });
    this.setState({ monuments: user.monuments || [] });
  }

  _getInitial = () => {
    const name = (this.state.user && this.state.user.name) || "";
    return name
      .split(" ")
      .map(n => n[0])
      .join("");
  };

  render() {
    // console.log("render");
    // console.log(this.props.user);

    const {
      user,
      isLogginRequire,
      isUserLogged,
      leftIcon,
      leftIconMethod,
      rightIcon,
      rightIconMethod,
      monuments
    } = this.state;

    return (
      <BasicPage
        isLogginRequire={isLogginRequire}
        isUserLogged={isUserLogged}
        icons={{ leftIcon, rightIcon }}
        methods={{ leftIconMethod, rightIconMethod }}
        navigation={this.props.navigation}
        loading={
          this.props.user.loading || user == undefined || monuments == undefined
        }
      >
        <View style={styles.avatarContainer}>
          <RNE.Avatar
            style={styles.avatar}
            large
            rounded
            title={user.picture ? "" : this._getInitial()}
            source={
              user.picture && {
                uri: user.picture
              }
            }
            activeOpacity={0.7}
          />
        </View>
        <ScrollView>
          <RNE.List>
            <ListItem
              title={i18n.t("basic.name")}
              subtitle={user.name}
              hideChevron={true}
            />
            <ListItem
              title={i18n.t("basic.email")}
              subtitle={user.email}
              hideChevron={true}
            />
          </RNE.List>

          <RNE.List>
            <ListOfMonuments
              monuments={monuments}
              author={user.name}
              navigation={this.props.navigation}
            />
          </RNE.List>
        </ScrollView>
      </BasicPage>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      dispatch(logout());
    }
  };
};

const mapStateToProps = state => ({
  i18n: state.i18n,
  user: state.user
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserScreen);
