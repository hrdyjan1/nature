/**
 * @file Style for list of monuments and user screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  titleView: {
    flexDirection: "row",
    justifyContent: "center"
  },
  titleText: {
    fontSize: 18,
    color: "#ccc"
  },
  subtitleView: {
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: 5
  },
  subtitleText: {
    textAlign: "center",
    color: "#000"
  },
  avatarContainer: {
    marginVertical: 10,
    alignItems: "center"
  }
});

export default styles;
