/**
 * @file Style for Home Screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  buttonStyle: {
    flex: 1,
    borderWidth: 2,
    backgroundColor: "red"
  }
});

export default styles;
