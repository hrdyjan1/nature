/**
 * @file Style for login screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";
import colors from "../features/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    alignSelf: "stretch",
    width: null,
    height: null
  },
  top: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  headingText: {
    textAlign: "center",
    color: colors.gray
  },
  bottom: {
    flex: 1
  },
  button: {
    margin: 5
  },
  buttonSignUp: {
    backgroundColor: "#68829E"
  },
  buttonSignIn: {
    backgroundColor: "#AEBD38"
  },
  buttonAnonymous: {
    backgroundColor: "#598234"
  }
});

export default styles;
