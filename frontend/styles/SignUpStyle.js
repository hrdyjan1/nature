/**
 * @file Style for sign up and sign in screen
 * @author Jan Hrdý <hrdyjan1@fel.cvut.cz, hrdyjan1@gmail.com>
 */

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    alignSelf: "stretch",
    width: null,
    height: null
  },

  top: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  middle: {
    flex: 3
  },
  bottom: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },

  button: {
    margin: 5
  },
  buttonConnectWithGoogle: {
    backgroundColor: "#EA4335"
  },
  buttonSignUp: {
    backgroundColor: "#68829E"
  },
  buttonAnonymous: {
    backgroundColor: "#598234"
  }
});

export default styles;
